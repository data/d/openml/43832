# OpenML dataset: Apple-Complete-Stock-Data1980-2020

https://www.openml.org/d/43832

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Apple has become a household name now a days as many people are using Apple products such as Iphone, Ipad, Apple watch etc. Apple recently became the only company to hit the 2 Trillion dollar mark which is a really great feat. But to be this big Apple had to start somewhere, even in stock market. So here we have the complete Data of Apple stock from its start from 1980 to 2020.
Content
This data set has 7 columns with all the necessary values such as opening price of the stock, the closing price of it, its highest in the day and much more. It has date wise data of the stock starting from 1980 to 2020.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43832) of an [OpenML dataset](https://www.openml.org/d/43832). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43832/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43832/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43832/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

